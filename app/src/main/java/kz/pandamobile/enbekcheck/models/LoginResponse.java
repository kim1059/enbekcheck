package kz.pandamobile.enbekcheck.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by kim1059 on 29.03.2017.
 */

public class LoginResponse {
    @SerializedName("token")
    private String token;
    @SerializedName("errors")
    private ArrayList<String> errors;

    public String getToken() {
        return token;
    }

    public ArrayList<String> getErrors() {
        return errors;
    }
}
