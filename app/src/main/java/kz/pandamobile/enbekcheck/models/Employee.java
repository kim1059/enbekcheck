package kz.pandamobile.enbekcheck.models;

/**
 * Created by kim1059 on 29.03.2017.
 */

public class Employee {
    private String name;

    public Employee(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
