package kz.pandamobile.enbekcheck.fragments;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.pandamobile.enbekcheck.R;
import kz.pandamobile.enbekcheck.activities.LoginActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {

    public SettingsFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @OnClick(R.id.llt_exit)
    public void onExitBtnClicked() {
        clearSharedPrefs();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
    }

    private void clearSharedPrefs() {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = shared.edit();
        editor.clear();
        editor.apply();
    }

}
