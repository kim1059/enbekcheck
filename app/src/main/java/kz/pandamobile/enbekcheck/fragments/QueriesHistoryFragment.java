package kz.pandamobile.enbekcheck.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.pandamobile.enbekcheck.R;
import kz.pandamobile.enbekcheck.R2;
import kz.pandamobile.enbekcheck.activities.EmployeeDetailActivity;
import kz.pandamobile.enbekcheck.adapters.EmployeesAdapter;
import kz.pandamobile.enbekcheck.models.Employee;

/**
 * A simple {@link Fragment} subclass.
 */
public class QueriesHistoryFragment extends Fragment
        implements EmployeesAdapter.ItemClickListener{

    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R2.id.pb_progress)
    ProgressBar pbProgress;
    @BindView(R2.id.tv_list_empty)
    TextView tvListEmpty;

    private List<Employee> employeeList = new ArrayList<>();
    private EmployeesAdapter adapter;

    public QueriesHistoryFragment() {
        // Required empty public constructor
    }

    public static QueriesHistoryFragment newInstance() {
        return new QueriesHistoryFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_queries_history, container, false);
        ButterKnife.bind(this, rootView);

        dummyData();
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvList.setLayoutManager(mLayoutManager);

        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(rvList.getContext(),
                mLayoutManager.getOrientation());
        rvList.addItemDecoration(mDividerItemDecoration);

        adapter = new EmployeesAdapter(getContext(), employeeList);
        rvList.setAdapter(adapter);
        adapter.setClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View view, int position) {
        Intent intent = new Intent(getActivity(), EmployeeDetailActivity.class);
        startActivity(intent);
    }

    private void dummyData() {
        for (int i=0; i<10; i++) {
            Employee employee = new Employee("Асель Сагатова-" + String.valueOf(i));
            employeeList.add(employee);
        }
    }

}
