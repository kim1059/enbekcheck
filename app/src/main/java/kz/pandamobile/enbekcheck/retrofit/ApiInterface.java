package kz.pandamobile.enbekcheck.retrofit;

import kz.pandamobile.enbekcheck.models.LoginResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by kim1059 on 29.03.2017.
 */

public interface ApiInterface {
    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> doLogin(@Field("login") String login, @Field("password") String password);

}
