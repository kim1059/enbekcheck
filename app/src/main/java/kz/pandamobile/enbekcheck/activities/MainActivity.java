package kz.pandamobile.enbekcheck.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import kz.pandamobile.enbekcheck.R;
import kz.pandamobile.enbekcheck.fragments.QueriesHistoryFragment;
import kz.pandamobile.enbekcheck.fragments.QueryFragment;
import kz.pandamobile.enbekcheck.fragments.SettingsFragment;
import kz.pandamobile.enbekcheck.utils.Constants;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView navigationView;
    private boolean viewIsAtHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        displayView(R.id.nav_query);
    }

    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("fragmentTag");
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, intent);
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        if (!viewIsAtHome) {
            displayView(R.id.nav_query);
        } else {
            Intent intent = new Intent(MainActivity.this, SplashScreenActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(Constants.INTENT_EXTRA_EXIT, true);
            startActivity(intent);
        }
    }

    public void displayView(int viewId) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String title = "";

        navigationView.setCheckedItem(viewId);
        switch (viewId) {
            case R.id.nav_query:
                fragmentManager.beginTransaction()
                        .replace(R.id.mainFrame, QueryFragment.newInstance(), "fragmentTag")
                        .addToBackStack(null)
                        .commit();
                title = getString(R.string.query);

                viewIsAtHome = true;
                break;
            case R.id.nav_history:
                fragmentManager.beginTransaction()
                        .replace(R.id.mainFrame, QueriesHistoryFragment.newInstance())
                        .addToBackStack(null)
                        .commit();
                title = getString(R.string.queries_history);

                viewIsAtHome = false;
                break;
            case R.id.nav_settings:
                fragmentManager.beginTransaction()
                        .replace(R.id.mainFrame, SettingsFragment.newInstance())
                        .addToBackStack(null)
                        .commit();
                title = getString(R.string.settings);

                viewIsAtHome = false;
                break;
        }

        // set the toolbar title
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        displayView(item.getItemId());
        return true;
    }


}
