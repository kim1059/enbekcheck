package kz.pandamobile.enbekcheck.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.pandamobile.enbekcheck.R;
import kz.pandamobile.enbekcheck.R2;
import kz.pandamobile.enbekcheck.models.LoginResponse;
import kz.pandamobile.enbekcheck.retrofit.ApiClient;
import kz.pandamobile.enbekcheck.retrofit.ApiInterface;
import kz.pandamobile.enbekcheck.utils.Constants;
import kz.pandamobile.enbekcheck.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    @BindView(R2.id.edt_login)
    EditText edtLogin;
    @BindView(R2.id.edt_password)
    EditText edtPassword;
    @BindView(R2.id.btn_login)
    Button btnLogin;
    @BindView(R.id.pb_progress)
    ProgressBar pbProgress;

    SharedPreferences shared;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        shared =  PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.login);
        }

        String result = Utils.signXML(getApplicationContext(), "123456");
        Log.e("result", result);
        if (!Utils.stringIsEmpty(shared.getString(Constants.LOGIN_TOKEN, ""))) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LoginActivity.this, SplashScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.INTENT_EXTRA_EXIT, true);
        startActivity(intent);
    }

    @OnClick(R.id.btn_login)
    public void onLoginBtnClicked() {
        String login = edtLogin.getText().toString();
        String password = edtPassword.getText().toString();

        Utils.hideSoftKeyboard(LoginActivity.this);

        // Reset errors.
        edtLogin.setError(null);
        edtPassword.setError(null);

        boolean cancel = false;
        View focusView = null;

        if (Utils.stringIsEmpty(login)) {
            edtLogin.setError(getString(R.string.can_not_be_null));
            focusView = edtLogin;
            cancel = true;
        } else if (Utils.stringIsEmpty(password) || !Utils.isPasswordValid(password)) {
            edtPassword.setError(getString(R.string.five_symbols_at_least));
            focusView = edtPassword;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            showProgress();
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<LoginResponse> call = apiService.doLogin(login, password);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse>call, Response<LoginResponse> response) {
                    hideProgress();
                    if (!Utils.stringIsEmpty(response.body().getToken())) {
                        saveToken(response.body().getToken());
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                    } else {
                        for (String error : response.body().getErrors()) {
                            Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse>call, Throwable t) {
                    hideProgress();
                    Toast.makeText(LoginActivity.this, getString(R.string.error_happened), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void saveToken(String token) {
        editor = shared.edit();
        editor.putString(Constants.LOGIN_TOKEN, token);
        editor.apply();
    }

    private void showProgress() {
        if (pbProgress == null)
            return;
        pbProgress.setVisibility(View.VISIBLE);
        btnLogin.setVisibility(View.GONE);
    }

    private void hideProgress() {
        if (pbProgress == null)
            return;
        pbProgress.setVisibility(View.GONE);
        btnLogin.setVisibility(View.VISIBLE);
    }

}
